#1 /bin/env/python
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 29 16:53:20 2013

@author: mark_mil@aad.gov.au

Quick mess around to see what the data from the micromodem colelcted during
Polarstern XXIX/7 voyage looks like

"""

from __future__ import print_function
import csv
import os
from matplotlib import pyplot as plt
import numpy
import datetime
import json

mission_folder = r'F:\ROV\wisky115'  # r'G:\ROV\wisky111'
infile = os.path.join(
    mission_folder, r'TechSAS\ROVMM\20130925-150752-micromodem.txt'
)
valeport_file = os.path.join(
    mission_folder, r'TechSAS\ROVVA\20130924-135641-valeport.txt'
)
conf_file = os.path.join(
    mission_folder, r'wisky115.conf'
)
ttt_data = []

#SNR threashold required to consider a ping as good....
SNR_THRESHOLD = 0
MAX_SPEED = 2.0
SOUND_SPEED = 1444.0
MAX_RATE = MAX_SPEED/SOUND_SPEED
MAX_Z_ERROR = 15


class transponder_system(object):
    """
    Class to hold information about the transponder system
    Reads from ROV-VIS conf files

    """

    def __init__(self, filename):
        with open(filename) as conf:
            data = json.load(conf)
        coords = []
        for name in ['TA', 'TB', 'TC']:
            coords.append(
                [data[name]['local_fixed'][x] for x in 'XY']
            )
        self.coords = numpy.array(coords)
        self.depth = data['TA']['local_fixed']['Z']
        translated = []
        self.offset = coords[0]
        for coord in self.coords:
            translated.append(coord - self.offset)
        translated = numpy.array(translated)
        print(translated)
        rotmat = numpy.matrix(
            [
                [translated[1, 0], -translated[1, 1]],
                [translated[1, 1],  translated[1, 0]]
            ]
        )
        self.rotmat = numpy.sqrt(2) * rotmat / numpy.linalg.norm(rotmat)

        rotated = numpy.matrix(translated) * self.rotmat
        print (rotated)
        self.d = rotated[1, 0]
        self.i = rotated[2, 0]
        self.j = rotated[2, 1]


class data_point(object):
    """
    Class to hold information about an individual data point

    """

    def __init__(self):

        self.tta = []
        self.snr = [0] * 4
        self.x = None
        self.y = None

    def average_speed_to(self, another_point):
        """
        Calculates the average speed between points, provided those points
        have been trilaterated.....
        Only 2 dimensions are taken into account.
        Units are m/s

        """

        vector = (
            numpy.array([self.x, self.y])
            - numpy.array([another_point.x, another_point.y])
        )
        distance = numpy.linalg.norm(vector)
        time_taken = self.timestamp - another_point.timestamp
        return distance/abs(time_taken.total_seconds())

    def clean(self):
        """
        Attempt to clean up data by removing the noisiest measurement
        and resetting the paramters calculated during trilateration.

        """

        self.tta[self.snr.index(min(self.snr))] = 0
        self.x, self.x, self.z = None, None, None

    def calc_tta(self, transponders):
        """
        Calculate travel times based on a potion. Used in interpolating data,
        but may also be used in testing

        """

        self.tta = []
        for coord in transponders.coords:
            vector = numpy.array([self.x, self.y]) - coord
            self.tta.append(numpy.linalg.norm(vector) / SOUND_SPEED)

    def trilaterate3(self, transponders):
        """
        Used to convert two-way travel times to an x/y location.
        Performs the 3D trilateration based ping timings alone

        """

        distances = [x*SOUND_SPEED for x in self.tta[:3]]
        x = (
            (distances[0] ** 2)
            - (distances[1] ** 2)
            + (transponders.d ** 2)
        )
        x = x / (2 * transponders.d)
        y = (
            (distances[0] ** 2)
            - (distances[2] ** 2)
            + (transponders.j ** 2)
            + (transponders.i ** 2)
        )
        y = y / (2 * transponders.j)
        y = y - (transponders.i / transponders.j) * x
        xy_array = (
            numpy.matrix([x, y]) * numpy.linalg.inv(transponders.rotmat)
        )
        xy_array = xy_array + transponders.offset
        self.x, self.y = [xy_array[0, i] for i in range(2)]
        z_squared = (distances[0] ** 2) - (y ** 2) - (x ** 2)
        if z_squared >= 0:
            self.z = numpy.sqrt(z_squared)
        else:
            self.clean()


def point_interpolate(p1, p2, time):
    """
    Do an interpolation between two data points that have been trilaterated

    """

    output = data_point()
    output.timestamp = time

    ratio = (
        (output.timestamp - p1.timestamp).total_seconds()
        / (p2.timestamp - p1.timestamp).total_seconds()
    )

    output.x = (p1.x + (p2.x - p1.x) * ratio)
    output.y = (p1.y + (p2.y - p1.y) * ratio)
    output.z = (p1.z + (p2.z - p1.z) * ratio)

    return output


#Read data from file
last_time = datetime.datetime.min

with open(infile) as datafile:
    for row in csv.reader(datafile):
        #Calculate timestamp
        try:
            timestamp = datetime.datetime.strptime(
                row[0], '%d/%m/%yT%H:%M:%S.%f',
            )
        except:
            continue

        #If this is a new timestamp, start a new data point
        if timestamp != last_time:
            last_time = timestamp
            this_point = data_point()
            this_point.timestamp = timestamp

        #Process matched filter data
        if row[1] == '$SNMFD':
            this_point.snr[int(row[2])-1] = float(row[5][:-3])/100.0

        if row[1] == '$SNTTA':
            tta_timestamp = datetime.datetime.strptime(
                row[0], '%d/%m/%yT%H:%M:%S.%f'
            )
            if tta_timestamp == this_point.timestamp:
                for record in row[2:6]:
                    try:
                        this_point.tta.append(float(record))
                    except ValueError:
                        this_point.tta.append(0)

            ttt_data.append(this_point)

last_good = [None]*4
last_good_time = [None]*4
dropped_snr = 0
dropped_rate = 0
rates = []

"""
#Remove points based on SNR and rate limits.
for point_index ,point in enumerate(ttt_data):
    for index, good in enumerate(last_good):
        if point.snr[index] < SNR_THRESHOLD:
            point.tta[index] = 0
            dropped_snr += 1
        if good is not None:
            time_elapsed = point.timestamp - last_good_time[index]
            rate = (
                (point.tta[index] - good) / time_elapsed.total_seconds()
            )
            rates.append(rate)
            if abs(rate) > MAX_RATE:
                ttt_data[point_index].tta[index] = 0
                dropped_rate += 1
        if point.tta[index] != 0:
            last_good[index] = point.tta[index]
            last_good_time[index] = point.timestamp
"""

#Now do an actual calculation of position
#Try simple trilateration without depth initially.
transponders = transponder_system(conf_file)
for point in ttt_data:
    if all(point.tta[:3]):
        #Calculate relative to transponder A
        point.trilaterate3(transponders)
"""
#Now we have the data points a little better... Re-evaluate the rates
last_good = None
dropped_rate = 0
rates = []
#Remove points based on rate limits.
for point in ttt_data:
    if point.x is not None:
        if last_good is not None:
            rate = point.average_speed_to(last_good)
            rates.append(rate)
            if rate > MAX_SPEED:
                point.clean()
            else:
                last_good = point
        else:
            last_good = point

#Now get rid of points with ridiculous depth solutions
#And record the indices of good solutions
for point in ttt_data:
    if point.x is not None:
        if abs(point.z + transponders.depth) > MAX_Z_ERROR:
            point.clean()
"""

#Now with tidied data... Have a go at creating intermediate solutions from
#incomplete ping sets.
last_good = None
next_good = None
err = []
for point_index, point in enumerate(ttt_data):
    if point.x is None:
        test_index = point_index + 1
        try:
            while ttt_data[test_index].x is None:
                test_index += 1
            next_good = ttt_data[test_index]
        except:
            next_good = None
        if next_good is None:
            guess = last_good
        elif last_good is None:
            guess = next_good
        else:
            guess = point_interpolate(last_good, next_good, point.timestamp)
    else:
        last_good = point

#Then filter a little using a simple 2D Kalman filter

#plt.close('all')
#plt.plot([x.tta for x in ttt_data])

#plt.figure()
#plt.plot(
#    [point.timestamp for point in ttt_data if point.x],
#    [[point.x, point.y, point.z] for point in ttt_data if point.x],
#)
plt.figure()
plt.plot(
    [point.x for point in ttt_data if point.x is not None],
    [point.y for point in ttt_data if point.x is not None],
    'r'
)
plt.grid()
plt.axis('equal')
