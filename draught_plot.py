# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 10:16:57 2013

Quick script to test the techsas_generator module, and to produce a nice plot
of the ice draught for the Sea Ice Physics team on Polarstern
@author: mark_mil
"""

from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import numpy as np
import csv

import techsas_generators


class valeport_draught(techsas_generators.techsas_generator):
    """
    Techsas generator that returns the interpolated valeport draught

    """

    DENSITY = 1024   # Density of sea water
    GRAVITY = 9.806  # m/s^2
    BAR = 1.01e5     # Conversion factor for Pa to Bar

    # Conversion factor between decibar and m of seawater
    CONV = -BAR / (10 * DENSITY * GRAVITY)
    OFFSET = 10

    THINNEST = 0
    FATTEST = -5

    def _calc_result(
        self, last_time, this_time, last_row, this_row, timestamp
    ):
        """
        This overwrites _calc_result and returns the ice draught. To be used
        in auto mode only. If it needs to do interpolation it raises
        ValueError.

        """

        if last_time == timestamp:
            depth = self.CONV * float(last_row[4]) + self.OFFSET
            ping_range = float(last_row[2])
            draught = depth + ping_range
            if self.FATTEST <= draught <= self.THINNEST:
                return draught
            else:
                return None
        else:
            raise ValueError

valeport_path = r'C:\Users\mark_mil\PolarsternData\ROV\wiskey104\TechSAS\ROVVA'
track_path = r'C:\Users\mark_mil\Documents\Spyder\positioner\104'

draught_data = valeport_draught(valeport_path, auto=True)
pos_data = techsas_generators.position_generator(track_path, auto=False)

with open('ridgedata.csv', 'w') as outfile:
    data_out = csv.writer(outfile, lineterminator='\n')
    plot_data = []
    for draught in draught_data:
        if draught[1] is None:
            continue
        timestamp = draught[0]
        if timestamp < pos_data.start:
            continue
        try:
            position = pos_data(timestamp)
            data_out.writerow(
                [timestamp.isoformat()] +
                list(position) +
                [draught[1]]
            )
            plot_data.append(position + (draught[1],))
        except StopIteration:
            break  # No more data

fig = plt.figure()
ax = fig.gca(projection='3d')
segments = np.array([plot_data[n:n + 2] for n in range(len(plot_data) - 1)])
lin_col = Line3DCollection(
    segments,
    cmap=cm.jet,
)
z = [plot_data[n][2] + plot_data[n + 1][2] for n in range(len(segments))]
lin_col.set_array(
    np.array(z)
)
big = segments.max(0).max(0)
small = segments.min(0).min(0)
ax.set_xlim(small[0], big[0])
ax.set_ylim(small[1], big[1])
ax.set_zlim(small[2], big[2])
ax.add_collection(lin_col)
plt.show()
