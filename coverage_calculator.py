# -*- coding: utf-8 -*-
"""
Created on Mon Oct 07 14:50:41 2013

@author: mark_mil
"""

from __future__ import print_function
import os
import re
import csv
import datetime
import numpy as np
from matplotlib import pyplot

BASE_DIR = r'C:\Users\mark_mil\PolarsternData\ROV'
RAMSES_ID = 'SAM_837a'

specs = 0

mission_names = ['wisky' + str(101+x) for x in range(19)]


def lookup_track(path):
    """
    Generator that returns ROV track position when sent a timestamp
    Not designed to be iterated over.

    """

    with open(path, 'r', 1) as track_file:
        input_lines = csv.reader(track_file)
        previous_row = input_lines.next()
        previous_row = input_lines.next()
        last_time = datetime.datetime.strptime(
            previous_row[0], '%Y-%m-%dT%H:%M:%S.%f'
        )
        timestamp = last_time
        for row in input_lines:
            try:
                this_time = datetime.datetime.strptime(
                    row[0], '%Y-%m-%dT%H:%M:%S.%f'
                )
            except ValueError:
                this_time = datetime.datetime.strptime(
                    row[0], '%Y-%m-%dT%H:%M:%S'
                )
            while timestamp < last_time:
                timestamp = (yield None)

            while last_time <= timestamp <= this_time:
                ratio = (
                    (timestamp - last_time).total_seconds()
                    / (this_time - last_time).total_seconds()
                )
                last_track = np.array([float(x) for x in previous_row[1:3]])
                this_track = np.array([float(x) for x in previous_row[1:3]])
                position = last_track + ratio * (this_track - last_track)
                timestamp = (yield position)

            last_time = this_time
            previous_row = row

positions = []
for mission in mission_names:
    position_file = mission + '_track.csv'
    try:
        file_list = os.listdir(os.path.join(BASE_DIR, mission))
    except:
        mission = mission.replace('wisky', 'wiskey')
        file_list = os.listdir(os.path.join(BASE_DIR, mission))

    for ramses_file in file_list:
        if re.search('.dat$', ramses_file):
            break

    ramses_file = os.path.join(BASE_DIR, mission, ramses_file)

    arm = False
    timestamps = []

    get_position = lookup_track(position_file)
    get_position.next()

    with open(ramses_file) as spec_file:
        for row in csv.reader(spec_file, delimiter=' '):
            if not arm:
                try:
                    if (row[0] == 'IDDevice') & (row[-1] == RAMSES_ID):
                        arm = True
                except IndexError:
                    pass
            else:
                try:
                    if (row[0] == 'DateTime'):
                        this_time = datetime.datetime.strptime(
                            ''.join(row[-2:]), '%Y-%m-%d%H:%M:%S'
                        )
                        if this_time not in timestamps:
                            timestamps.append(this_time)
                            try:
                                pos = get_position.send(this_time)
                            except StopIteration:
                                break
                            if pos is not None:
                                positions.append(pos)
                        arm = False

                except IndexError:
                    pass
    specs += len(timestamps)
    print(mission, len(timestamps), specs)

pyplot.plot([pos[0] for pos in positions], [pos[1] for pos in positions], 'x')
pyplot.grid()
pyplot.axis('equal')
