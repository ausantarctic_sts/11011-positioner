#1 /bin/env/python
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 2, 2013

@author: mark_mil@aad.gov.au

Ambitious effort to perform non-linear Kalman filtering in data collected
during the Polarstern XXIX/7 voyage

"""

from __future__ import print_function
import csv
import os
from matplotlib import pyplot as plt
import numpy as np
import datetime
import json


def get_techsas_file_start(path):
    """
    Function to find the first timestamp in a techsas text file

    """

    with open(path) as techsas_file:
        text = techsas_file.read(21)
    return datetime.datetime.strptime(text, '%d/%m/%yT%H:%M:%S.%f')


def techsas_sorted_listdir(path):
    """
    Function that returns a list of techsas files on a particular path that is
    sorted such that the first file in the list contains the earliest data

    """

    file_list = os.listdir(path)

    #Get list of files and sort by start time
    for index, fn in enumerate(file_list):
        start_time = get_techsas_file_start(os.path.join(path, fn))
        file_list[index] = (start_time, fn)
    file_list.sort()
    return [tup[1] for tup in file_list]


class transponder_system(object):
    """
    Class to hold information about the transponder system
    Reads from ROV-VIS conf files

    """

    def __init__(self, filename):
        with open(filename) as conf:
            data = json.load(conf)
        coords = []
        for j in 'XYZ':
            coords.append(
                [data[name]['local_fixed'][j] for name in ['TA', 'TB', 'TC']]
            )
        self.x, self.y, self.z = coords
        self.holex = data['ROV_HOLE']['local_fixed']['X']
        self.holey = data['ROV_HOLE']['local_fixed']['Y']


class observation(object):
    """
    Object for holding what is called an observation.
    It includes the valeport data and up to four trandponder ping timings.
    It could potentially be expanded to include gyro, compass or even pitch
    and roll sensors.

    """

    def __init__(self, timestamp, pings, transponders):
        self.timestamp = timestamp
        self.valeport = None
        self.pings = pings
        self.transponders = transponders

    def construct_matrices(self, xsubk):
        """
        Generate the H matrix used to predict the state of the ROV (in Kalman
        filter terms) based on the observations.
        Also generate the associated observation vector (usually denoted z
        subscript k), and have a go at v... i.e. estimate variance of
        measurements. And hence a rudimentary guess at R.
        Input is the previous estimate of position, around which H is
        linearised.

        """

        xsubk = np.mat(xsubk)

        self.zsubk = {}
        self.H = []
        self.v = []

        if self.valeport:
            self.zsubk['valeport'] = (10 + CONV*self.valeport)
            self.H.append([0, 0, 1, 0, 0, 0])
            self.v.append(VALEPORT_STD)

        for index, ping in enumerate(self.pings[0:3]):
            if ping:
                key = "ping{}".format(index)
                self.zsubk[key] = (SOUND_SPEED * ping)
                self.H.append([
                    (xsubk[0, 0] - transponders.x[index]) / (self.zsubk[key]),
                    (xsubk[0, 1] - transponders.y[index]) / (self.zsubk[key]),
                    (xsubk[0, 2] - transponders.z[index]) / (self.zsubk[key]),
                    0, 0, 0
                ])
                self.v.append(PING_STD)
        self.H = np.array(self.H)
        self.R = np.diag([x ** 2 for x in self.v])


class estimator(object):
    """
    Class containing the bit that does the Kalman filter hard yards

    """

    STD_HORZ = 0.07   # Standard deviation of acceleration in horizontal
    STD_VERT = 0.1   # Standard deviation of acceleration in vertical
    MAX_SPEED = 1.0  # Ignore any iteration that suggests unrealistic speed
    MAX_JUMP = 200    # Ignore iterations that cause big position jumps
    STD_HORZ_1D = STD_HORZ / np.sqrt(2)

    def __init__(self, initial_state):
        self._last_stamp = None
        self.state = np.array(initial_state)
        self.dev = []
        self.P = 5*np.eye(6)  # See how this initialisation goes????

    def update(self, ob):
        """
        Update estimator based on an observations

        """

        damping = 0.1  # Most simplistic damping..... This is the fraction of
                       # speed expected to be wiped off each second

        if self._last_stamp is None:
            #First data point.... Calculate original pings.
            self.last_zsubk = ob.zsubk
            self._last_stamp = ob.timestamp
        else:
            dt = (ob.timestamp - self._last_stamp).total_seconds()
            step_damping = min(damping*dt, 1)
            temp = (1 - step_damping)
            temp2 = (2 - step_damping) * dt / 2

            self.F = np.mat([
                [1,      0,      0, temp2,     0,     0],
                [0,      1,      0,     0, temp2,     0],
                [0,      0,      1,     0,     0, temp2],
                [0,      0,      0,  temp,     0,     0],
                [0,      0,      0,     0,  temp,     0],
                [0,      0,      0,     0,     0,  temp],
            ])

            temp = (dt ** 2) / 2
            a = (temp * self.STD_HORZ_1D) ** 2
            b = dt * temp * (self.STD_HORZ_1D ** 2)
            c = 2 * temp * (self.STD_HORZ_1D ** 2)
            d = dt * temp * (self.STD_VERT ** 2)
            e = (temp * self.STD_VERT) ** 2
            f = 2 * temp * (self.STD_VERT ** 2)

            self.Q = np.mat([
                [a, 0, 0, b, 0, 0],
                [0, a, 0, 0, b, 0],
                [0, 0, e, 0, 0, d],
                [b, 0, 0, c, 0, 0],
                [0, b, 0, 0, c, 0],
                [0, 0, d, 0, 0, f],
            ])

            poss_keys = ['valeport', 'ping0', 'ping1', 'ping2']

            # Here comes the heavy lifting....
            # Predict:
            predict_state = self.state * self.F.transpose()
            predict_P = self.F * self.P * self.F.transpose() + self.Q

            # Determine the optimal gain:
            predict_z = []
            observed_z = []
            for index, key in enumerate(poss_keys):
                if key in ob.zsubk:
                    observed_z.append(ob.zsubk[key])
                    if key == 'valeport':
                        predict_z.append(predict_state[0, 2])
                    else:
                        trans = index - 1
                        vector = ([
                            ob.transponders.x[trans] - predict_state[0, 0],
                            ob.transponders.y[trans] - predict_state[0, 1],
                            ob.transponders.z[trans] - predict_state[0, 2],
                        ])
                        predict_z.append(np.linalg.norm(vector))

            residual = (
                np.array(observed_z) - np.array(predict_z)
            )
            innovation = (ob.H * predict_P * ob.H.transpose()) + ob.R
            gain = predict_P * ob.H.transpose() * np.linalg.inv(innovation)

            # Update predicted:
            residual = np.mat(residual)
            temp = (
                predict_state.transpose()
                + (gain * residual.transpose())
            )
            change = self.state - temp.transpose()
            distance = np.linalg.norm(change[0:2])
            distance_limit = min(self.MAX_JUMP, self.MAX_SPEED*dt)
            if distance <= distance_limit:
                self.state = temp
                self._last_stamp = ob.timestamp
                self.state = np.array(self.state.transpose())[0]
                self.P = (np.eye(6) - (gain * ob.H)) * predict_P
                self.dev.append(np.sqrt(self.P[0, 0] + self.P[1, 1]))


def ping_generator(path, transponders):
    """
    Generator that returns pings from WHOI micromodem files. All files in the
    path are process in chronological order.

    """

    files = techsas_sorted_listdir(path)
    for filename in files:
        full_path = os.path.join(path, filename)
        with open(full_path, 'r', 1) as mm_file:
            for row in csv.reader(mm_file):
                if row[1] == '$SNTTA':
                    timestamp = datetime.datetime.strptime(
                        row[0], '%d/%m/%yT%H:%M:%S.%f'
                    )
                    pings = []
                    for record in row[2:5]:
                        try:
                            pings.append(float(record))
                        except ValueError:
                            pings.append(None)
                    if pings.count(None) < 3:
                        last_ob = observation(timestamp, pings, transponders)
                        yield last_ob


def lookup_valeport(path):
    """
    Generator that returns valeport depths when sent a timestamp
    not designed to be iterated over. It processes all the files in path in
    chronological order

    """

    files = techsas_sorted_listdir(path)
    for filename in files:
        full_path = os.path.join(path, filename)
        with open(full_path, 'r', 1) as val_file:
            input_lines = csv.reader(val_file)
            previous_row = input_lines.next()
            last_time = datetime.datetime.strptime(
                previous_row[0], '%d/%m/%yT%H:%M:%S.%f'
            )
            timestamp = last_time
            for row in input_lines:
                this_time = datetime.datetime.strptime(
                    row[0], '%d/%m/%yT%H:%M:%S.%f'
                )
                while last_time <= timestamp <= this_time:
                    ratio = (
                        (timestamp - last_time).total_seconds()
                        / (this_time - last_time).total_seconds()
                    )
                    last_depth = float(previous_row[4])
                    this_depth = float(row[4])
                    depth = last_depth + ratio * (this_depth - last_depth)
                    timestamp = (yield depth)

                last_time = this_time
                previous_row = row

mission = r'wisky108'
mission_folder = os.path.join(r'C:\Users\mark_mil\PolarsternData\ROV', mission)

infile_path = os.path.join(
    mission_folder, r'TechSAS\ROVMM'
)

valeport_path = os.path.join(
    mission_folder, r'TechSAS\ROVVA'
)

conf_file = os.path.join(
    mission_folder,  mission + '.conf'
)

DENSITY = 1024   # Density of sea water
GRAVITY = 9.806  # m/s^2
BAR = 1.01e5     # Conversion factor for Pa to Bar

# Conversion factor between decibar and m of seawater
CONV = -BAR / (10 * DENSITY * GRAVITY)
SOUND_SPEED = 1444

VALEPORT_STD = 0.05
PING_STD = 5.0

transponders = transponder_system(conf_file)
depth_generator = lookup_valeport(valeport_path)
kalman = estimator([transponders.holex, transponders.holey, 0, 0, 0, 0])
depth_generator.next()  # Start generator
track = []

trackfile = mission + '_track.csv'
with open(trackfile, 'w') as outfile:
    data_sink = csv.writer(outfile, lineterminator='\n')
    headings = [
        'timestamp',
        'x position', 'y_position', 'depth',
        'x_velocity', 'y_velocity', 'z_velocity',
        'x_std', 'y_std', 'z_std'
    ]
    data_sink.writerow(headings)
    for ob in ping_generator(infile_path, transponders):
        try:
            ob.valeport = depth_generator.send(ob.timestamp)
        except StopIteration:
            ob.valeport = None
        ob.construct_matrices(kalman.state)
        kalman.update(ob)
        track.append(kalman.state)
        row = (
            [ob.timestamp.isoformat()] +
            list(kalman.state[0:6]) +
            [np.sqrt(kalman.P[x, x]) for x in range(3)]
        )
        data_sink.writerow(row)

plt.close('all')
plt.plot(track)
plt.figure()

plt.plot([a[0] for a in track], [a[1] for a in track], '-b')
plt.grid()
plt.axis('equal')

plt.figure()
plt.plot(kalman.dev)
