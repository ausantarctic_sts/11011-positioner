# -*- coding: utf-8 -*-
"""
Created on Tue Oct 08 11:16:14 2013
Script to plot all of the ice camp 2 under-ice ROV excursions.
Different plots shown in different colours.

@author: mark_mil
"""

from matplotlib import pyplot, cm
import numpy as np
import csv
import datetime

tracks = 19

mission_names = ['wisky' + str(101+x) for x in range(tracks)]
cmap = [cm.get_cmap('jet')(256*x/(tracks - 1)) for x in range(tracks)]

w,h = pyplot.figaspect(4.0/5.0)
fig = pyplot.figure(figsize = (w,h))
ax = fig.add_axes()
pyplot.grid()
pyplot.axis('equal')

def time_from_iso(iso_string):
    """
    Python's inbuilt datetime has the isoformat() method to create timestamp
    strings, but it does not have the inverse. And it truncates the string
    if the time happens to fall on a mutliple of 1 second!!! This is the
    inverse fuction. Takes a string and returns a datetime
    
    """
    try:
        time = datetime.datetime.strptime(
            iso_string, '%Y-%m-%dT%H:%M:%S.%f'
        )
    except ValueError:
        time = datetime.datetime.strptime(
            iso_string, '%Y-%m-%dT%H:%M:%S'
        )
    return time
    
for colour, mission in zip(cmap, mission_names):   
    
    position_file = mission + '_track.csv'
    with open(position_file) as infile:
        input_data = csv.reader(infile)
        input_data.next() #Skip headings....
        x = []
        y = []
        start_time = None
        for row in csv.reader(infile):
            if not start_time:
                start_time = time_from_iso(row[0])
            x.append(float(row[1]))
            y.append(float(row[2]))
    
    mission_time = time_from_iso(row[0]) - start_time
    last_point = ()
    distance = 0
    for point in zip(x, y):
        if last_point:
            distance += (
                np.linalg.norm(np.array(point) - np.array(last_point))
            )
        last_point = point
    print mission_time, distance
    data_label = mission +"-{:.0f}m".format(distance)
    pyplot.plot(x, y, color = colour, label = data_label)

pyplot.legend()
pyplot.xlabel('x co-ordinate (m)')
pyplot.ylabel('y co-ordinate (m)')            
pyplot.title('ROV Tracks During WISKY Icecamp #2')                